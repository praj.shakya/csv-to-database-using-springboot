package com.prajwal.code.controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.prajwal.code.entity.Person;
import com.prajwal.code.repository.PersonRepository;
@Controller
@RequestMapping(value = "/")
public class PersonController {
	@Autowired
	PersonRepository repository;
	
	@GetMapping
	@ResponseBody
	public String importData() {
		try {
			BufferedReader reader=new BufferedReader(new FileReader("C:\\Users\\User\\eclipse-workspace\\CsvtoDatabase2\\persons.csv"));
			String line=reader.readLine();
			while((line=reader.readLine())!=null) {
				String[] data= line.split(",");
				
				Person person=new Person();
				if(data.length>=26) {
				person.setEventName(data[0]);
				person.setScannedDate(data[1]);
				person.setScannedTime(data[2]);
				person.setFirstName(data[3]);
				person.setMiddleInitial(data[4]);
				person.setLastName(data[5]);
				person.setEmail(data[6]);
				person.setEmail2(data[7]);
				person.setCompanyName(data[8]);
				person.setJobTitle(data[9]);
				person.setAddress(data[10]);
				person.setAddress2(data[11]);
				person.setAddress3(data[12]);
				person.setCity(data[13]);
				person.setStateId(data[14]);
				person.setZip(data[15]);
				person.setCountryId(data[16]);
				person.setPhoneNumber(data[17]);
				person.setPhoneNumber2(data[18]);
				person.setFaxNumber(data[19]);
				person.setQuestion(data[20]);
				person.setResponse(data[21]);
				person.setNote(data[22]);
				person.setCollateral(data[23]);
				person.setQd(data[24]);
				person.setScannedBy(data[25]);
				
				
				System.out.println(person);
				repository.save(person);
				}
			}
			reader.close();
		}catch(IOException ioe) {
			System.out.println(ioe);
			return "failed";
		}
		return "success";
	}
}
