package com.prajwal.code.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prajwal.code.entity.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Integer> {

}
